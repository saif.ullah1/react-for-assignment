import { useState } from "react";
import Details from "./components/details-components/details";
import Form from "./components/form-components/form";
import './App.css'

const App=()=>{
  const[userDetails,setUserDetails]=useState([]);

  const onSubmit=(e)=>{
    e.preventDefault();

    const dataClone=[...userDetails];
    const firstName=e.target.firstName.value;
    console.log(firstName);
    const data={
      firstName:e.target.firstName.value,
      surName:e.target.surName.value,
      age:e.target.age.value,
      email:e.target.email.value
    }
    dataClone.push(data);
    setUserDetails(dataClone);
    e.target.reset();
  }


  const deleteHandler=(data)=>{
    const dataClone=[...userDetails];
    const index=dataClone.findIndex((user)=>user.firstName===data.firstName);
    dataClone.splice(index,1);
    setUserDetails(dataClone);
  }
  return(
    <main className="main">
      <Form
      submit={onSubmit}
      />
      <Details
      userData={userDetails}
      onDelete={deleteHandler}/>
    </main>
  )
}

export default App;