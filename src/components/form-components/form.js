import './form.css'
const Form=({submit})=>{
    return(
        <div className="form-container">
            
            <form onSubmit={submit} className="form">
            <h3>Details</h3>
                <div className="formField">
                    <input type="text" placeholder="First-Name" name="firstName"></input>
                </div>
                <div className="formField">
                    <input type="text" placeholder="sur-Name" name="surName"></input>
                </div>
                <div className="formField">
                    <input type="number" placeholder="Age" name="age"></input>
                </div>
                <div className="formField">
                    <input type="email" placeholder="email" name="email"></input>
                </div>
                <div className="btn">
                    <button type="submit" >Submit</button>
                </div>
            </form>
        </div>
    )
}

export default Form;