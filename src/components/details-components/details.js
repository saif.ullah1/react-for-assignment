import Card from "../card-components/card";
import './detais.css'
const Details=({userData,onDelete})=>{
    return(
        <div className="detail-section">
            {userData.map((u,i)=>{
                return(
                    <Card
                    key={i}
                    userData={u}
                    deleteHandler={onDelete}
                    />
                )              
                
            })}
        </div>
    )
}

export default Details;