import './card.css'
const Card=({userData,deleteHandler})=>{
return(
    <div className='card'>
        <div className='card-value'>
        <span className='value'>Name: {userData.firstName} {userData.lastName}</span>            
            <span className='value'>Age: {userData.age}</span>
            <span className='value'>Email: {userData.email}</span>

        <button onClick={()=>deleteHandler(userData)}>Delete</button>
        </div>
    </div>
)
}

export default Card;